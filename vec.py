from numpy import array, matmul, inner, outer, tensordot
from fix import Fix

class Vec():
	def __init__(self, data):
		self.data = array(data, dtype=object)
		self.__array_interface__ = self.data.__array_interface__

m3 = array([
	[1,2,0],
	[0,1,2],
	[0,0,1]], dtype=float)

f2 = array([1,2,3], dtype=float)
f3 = Vec([Fix(1), Fix(2), Fix(3)])

print(matmul(m3, f3))
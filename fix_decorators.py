# Decorator to generate non-inplace operators from inplace ones
def derrive_non_inplace(cls):
	def make_operator(opname):
		def newop(self, rhs):
			new = self.copy()
			result = getattr(new, opname)(rhs)

			return result

		return newop

	cls.__floordiv__ = make_operator("__ifloordiv__")
	cls.__add__ = make_operator("__iadd__")
	cls.__sub__ = make_operator("__isub__")
	cls.__mul__ = make_operator("__imul__")

	return cls

def make_arith_guard(adjust=True):
	def arith_guard(func):
		def inner(self, rhs, *args, **kwargs):
			rhs = self.coerce_self(rhs, adjust=adjust)
			result = func(self, rhs, *args, **kwargs)
			self.assert_no_fuckery()

			return result

		return inner

	return arith_guard
# Signed fixed point data type
from functools import total_ordering
from fix_decorators import derrive_non_inplace, make_arith_guard

@derrive_non_inplace
@total_ordering
class Fix():
	def assert_no_fuckery(self):
		assert self.intvalue < (1 << (self.bits-1)) and self.intvalue >= -(1 << (self.bits-1)), \
			f"Value overflows {self.intvalue} -> {self.bits} signed bits"

		assert type(self.intvalue) is int
		assert self.fracbits >= 0, f"Fracbits must be non-negative: {self.fracbits}"
		assert self.bits > 0, f"Bits must be positive: {self.bits}"

	# Try to left shift other Fix rhs to the precision of self
	def adjust_to_self(self, rhs):
		assert type(rhs) is type(self)
		assert rhs.fracbits <= self.fracbits, \
			f"Can't left-shift {rhs.notation()} to match {self.notation()}"

		# Adjust radix
		dif = self.fracbits - rhs.fracbits
		rhs.fracbits += dif
		rhs.intvalue *= 1 << dif
		rhs.bits += dif

		return rhs

	# Adjust size up only
	def adjust_overall(self, bits, fracbits):
		assert bits >= self.bits and fracbits >= self.fracbits, "Can't reduce precision"

		fdif = fracbits - self.fracbits
		tdif = bits - self.bits

		self.fracbits += fdif
		self.bits += tdif
		self.intvalue *= 1 << fdif

		return self

	# Coerce float to smallest representation in Fix
	def coerce_smallest(self, rhs):
		fracbits = 0
		minbits = 0

		while rhs % 1 != 0.0:
			fracbits += 1
			minbits += 1 
			rhs *= 2.0

		new = Fix(rhs, bits=128, fracbits=0)
		new.fracbits = fracbits
		new.bits = max(new.intvalue.bit_length() + 1, minbits)

		return new

	# Coerce a value to be compatible for arrithmetic
	# Literal int and float values should be coerced the the smallest possible size
	def coerce_self(self, rhs, adjust=True):
		if type(rhs) is float or type(rhs) is int:
			rhs = self.coerce_smallest(rhs)

			if adjust:
				rhs = self.adjust_to_self(rhs)

		elif type(rhs) is type(self):
			if adjust:
				rhs = self.adjust_to_self(rhs.copy())

		else:
			assert False, "No possible coercion to Fix type"

		return rhs

	# Return a string representing the fixed point style
	def notation(self):
		return f"{self.bits-self.fracbits}.{self.fracbits}"

	def copy(self):
		new = Fix(0, bits=self.bits, fracbits=self.fracbits)
		new.intvalue = self.intvalue
		return new

	def __init__(self, value, bits=None, fracbits=None):
		# Branch to copy constructor
		if type(value) is type(self):
			bits = value.bits
			fracbits = value.fracbits
			intvalue = value.intvalue
		elif bits is None and fracbits is None:
			# Calculate bits automatically
			value = self.coerce_smallest(value)
			bits = value.bits
			fracbits = value.fracbits
			intvalue = value.intvalue
		else:
			# Value may be an int or float
			intvalue = value * 2 ** fracbits
			assert intvalue // 1 == intvalue, \
				f"Conversion to fixed point is inexact {value} -> {fracbits} fraction bits"

		self.bits = bits
		self.fracbits = fracbits
		self.intvalue = int(intvalue)

		self.assert_no_fuckery()

	def __float__(self):
		return self.intvalue / (1 << self.fracbits)

	def __format__(self, formatter):
		return float.__format__(float(self), formatter)

	def __repr__(self):
		return f"{self:0.3f}"

	@make_arith_guard(adjust=True)
	def __eq__(self, rhs):
		return self.intvalue == rhs.intvalue

	@make_arith_guard(adjust=True)
	def __le__(self, rhs):
		return self.intvalue <= rhs.intvalue

	@make_arith_guard(adjust=True)
	def __iadd__(self, rhs):
		self.intvalue += rhs.intvalue
		self.adjust_overall(
			max(self.bits, rhs.bits) + 1, 
			self.fracbits)

		return self

	def __radd__(self, rhs):
		return self.__add__(rhs)

	@make_arith_guard(adjust=False)
	def __imul__(self, rhs):
		self.bits += rhs.bits 
		self.fracbits += rhs.fracbits
		self.intvalue *= rhs.intvalue
		
		return self

	def __rmul__(self, rhs):
		return self.__mul__(rhs)

	def __ilshift__(self, rhs):
		if rhs > 0:
			self.bits = max(self.bits + rhs, 0)
			self.intvalue *= 2
		elif rhs < 0:
			self.fracbits = max(self.fracbits - rhs, 0)

		self.assert_no_fuckery()

		return self

	def __irshift__(self, rhs):
		self <<= -rhs
		return self

	def __isub__(self, rhs):
		self += -rhs
		return self

	@make_arith_guard(adjust=False)
	def __ifloordiv__(self, rhs):
		assert rhs != 0 and not self == 0, "Divided Fix by zero"
		tmp = rhs.copy()

		if tmp.fracbits < self.fracbits:
			self.adjust_to_self(tmp)
		elif self.fracbits < tmp.fracbits:
			tmp.adjust_to_self(self)

		# Make room for worst case big number
		self.intvalue <<= tmp.fracbits
		self.bits += tmp.fracbits
		self.intvalue //= tmp.intvalue

		# Floor
		self.intvalue &= -(1 << self.fracbits)

		return self

	# Negate, doesn't overflow
	# Note that unary negate is fundamentally not in-place
	def __neg__(self):
		new = self.copy()
		new.intvalue *= -1

		new.assert_no_fuckery()

		return new
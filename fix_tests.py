from fix import Fix
import random
import time

# Basic coercion from ints and floats
def testnumbers(n, allowzero):
	print("Coercion")
	numbers = []

	for i in range(n):
		power = random.randrange(-10, 0)
		num = 2 ** power * random.randrange(-1000000, 1000000) / 256.0
		f = Fix(num)

		if allowzero or f != 0.0:
			numbers.append(f)
			assert num == f, "Coercion from float inaccurate"

	return numbers

testa = testnumbers(300000, True)
testb = testnumbers(300000, False)

# Dummy because python builtins don't follow Python's own rules
def float_op(a, b, opname):
	assert type(a) is float and type(b) is float, "Misuse of float_op"

	if opname == "__add__":
		return a + b
	if opname == "__sub__":
		return a - b
	if opname == "__mul__":
		return a * b
	if opname == "__floordiv__":
		return a // b
	else:
		assert False, "Invalid float op"

# Test an operator with the given sets of Fix
def test_operator(testa, testb, opname):
	for a, b in zip(testa, testb):
		# Adds require more precise op on the left
		if (opname == "__add__" or opname == "__sub__") and b.fracbits > a.fracbits:
			tmp = a
			a = b
			b = tmp

		answer = float_op(float(a), float(b), opname)
		result = getattr(a, opname)(b)

		# print(f"Check: {a} ({a.notation()}) {opname[2:-2]} {b} ({b.notation()}) -> "\
		#	+ f"{result} ({result.notation()})")

		assert(answer == result), \
			f"Result inaccurate: {answer} != {result} ({result.notation()})"

tstart = time.monotonic()

test_operator(testa, testb, "__add__")
test_operator(testa, testb, "__sub__")
test_operator(testa, testb, "__mul__")
test_operator(testa, testb, "__floordiv__")
print("Tests passed")

tend = time.monotonic()

print(f"Tests completed in {tend - tstart}")